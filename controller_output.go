package restorator

import (
	"net/http"
)

type ControllerOutput struct {
	Status  int
	Headers http.Header
	Data    interface{}
}

func NewControllerOutput() ControllerOutput {
	co := ControllerOutput{}
	co.Headers = http.Header(make(map[string][]string))
	co.Data = nil
	return co
}

func (co ControllerOutput) write(res http.ResponseWriter) {
	for key, _ := range map[string][]string(co.Headers) {
		res.Header().Set(key, co.Headers.Get(key))
		println(key)
		println(res.Header().Get(key))
	}
	res.WriteHeader(co.Status)
	res.Write(co.Data.([]byte))
	return
}

func (co ControllerOutput) set(data []byte, status int) ControllerOutput {
	co.Data = data
	co.Status = status
	return co
}
