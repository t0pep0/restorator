package marshall

import (
	"encoding/json"
)

type JSON struct {
}

func (j JSON) GetAcceptReqValue() string {
	return "application/json"
}

func (j JSON) Marshall(v interface{}) (res []byte, err error) {
	return json.Marshal(v)
}

func (j JSON) Unmarshall(raw []byte, v interface{}) (err error) {
	return json.Unmarshal(raw, v)
}
