package marshall

import (
	"encoding/xml"
)

type XML struct {
}

func (x XML) GetAcceptReqValue() string {
	return "application/xml"
}

func (x XML) Marshall(v interface{}) (res []byte, err error) {
	return xml.Marshal(v)
}

func (x XML) Unmarshall(raw []byte, v interface{}) (err error) {
	return xml.Unmarshal(raw, v)
}
