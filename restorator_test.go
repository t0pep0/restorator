package restorator

import (
	"testing"
)

var r = new(Restorator)

func TestGetActions(t *testing.T) {
	table := map[string]_Action{
		"action/1":        _Action{Name: "action", Identifiers: []int64{1}},
		"/action/1/":      _Action{Name: "action", Identifiers: []int64{1}},
		"/action//1//":    _Action{Name: "action", Identifiers: []int64{1}},
		"/action/1,2,3,4": _Action{Name: "action", Identifiers: []int64{1, 2, 3, 4}},
	}
	for input, res := range table {
		out := r.get_Actions(input)
		if out.Name != res.Name {
			t.Errorf("Name mismatch! %v != %v", out, res)
		}
		if len(out.Identifiers) != len(out.Identifiers) {
			t.Errorf("Length mismatch! %v != %v", out, res)
		}
		for i2, id := range out.Identifiers {
			if id != res.Identifiers[i2] {
				t.Errorf("Id mismatch! %v != %v", out, res)
			}
		}
	}
}

func BenchmarkGetActions(b *testing.B) {
	for i := 0; i < b.N; i++ {
		r.get_Actions("/action/1,2,3,4,556757,765")
	}
}
