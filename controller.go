package restorator

type ControllerGenerator func() Controller

type Controller interface {
	//Name, it's use in routing
	GetName() string
	//List of interestr Header
	InterestedHeader() []string
	//List of requeres header
	RequeredHeader() []string
	//List of header may used in response
	ResponseHeader() []string
	//List of interest  url-query params
	InterestedQuery() []string
	//List of needed url-query values
	RequeredQuery() []string
	//Only POST if empty - upload not allowed
	UploadFileName() string
	//List of fields in response struct
	Fields() []string
	//Controller provide this request method?
	Can(method string) bool
	//This method must return a pointer to empty struct, for fill this struct from request.Body
	GetBodyStruct() interface{}
	Get(input ControllerInput) (output ControllerOutput, err error)
	Put(input ControllerInput) (output ControllerOutput, err error)
	Delete(input ControllerInput) (output ControllerOutput, err error)
	Post(input ControllerInput) (output ControllerOutput, err error)
}
