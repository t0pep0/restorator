package restorator

type Marshall interface {
	GetAcceptReqValue() string
	Marshall(v interface{}) (res []byte, err error)
	Unmarshall(raw []byte, v interface{}) (err error)
}
