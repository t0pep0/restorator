package restorator

import (
	"bitbucket.org/t0pep0/restorator/helpers"
	"fmt"
	"github.com/t0pep0/go-pcre"
	"io/ioutil"
	"net/http"
)

//secHeader is Headers.Get("X-Secure")
type AccessValidationFunc func(path string, secHeader string) bool
type _Action struct {
	Name        string
	Identifiers []int64
}

type Restorator struct {
	rex         pcre.Regexp
	marshalls   map[string]Marshall
	controllers map[string]ControllerGenerator
	secureFunc  AccessValidationFunc
}

func NewRestorator() (r *Restorator, err error) {
	r = new(Restorator)
	r.rex, err = pcre.Compile(`\/([a-zA-Z]*)[\/]*([0-9,]*)`, pcre.UTF8|pcre.UCP)
	r.controllers = make(map[string]ControllerGenerator)
	r.marshalls = make(map[string]Marshall)
	r.secureFunc = nil
	if err != nil {
		return nil, err
	}
	return r, nil
}

const (
	expectedSliceLength      = 3
	countBytesAddToNormalize = 2
)

func (r *Restorator) get_Actions(path string) _Action {
	tmp := helpers.SplitString(path, '/')
	if len(tmp) > 1 {
		return _Action{Name: tmp[0], Identifiers: helpers.SplitInt64Value(tmp[1], ',')}
	} else if len(tmp) == 1 {
		return _Action{Name: tmp[0], Identifiers: []int64{}}
	} else {
		return _Action{Name: "", Identifiers: []int64{}}
	}
}

func (r *Restorator) RegisterMarshall(m Marshall) {
	r.marshalls[m.GetAcceptReqValue()] = m
}

func (r *Restorator) RegisterController(c ControllerGenerator) {
	r.controllers[c().GetName()] = c
}

const (
	unknownMarshallStr       = "Requested unknown format!"
	unknownMarshallCode      = 400
	unknownControllerStr     = "Requested unknown controller!"
	unknownControllerCode    = 404
	noAccessStr              = "Access denied!"
	noAccessCode             = 401
	controllerErrorCode      = 500
	methodNotAlowerErrorStr  = "This controller not provide this method!"
	methodNotAlowerErrorCode = 405
	marshallErrorCode        = 502
	requeredNotSentErr       = 412
	_OK                      = 200

	_GET    = "GET"
	_POST   = "POST"
	_DELETE = "DELETE"
	_PUT    = "PUT"

	accept      = "accept"
	contentType = "content-type"
	textPlain   = "text/plain"

	xSecureHeader = "X-Secure"
)

func (r *Restorator) RegisterAccessValidaionFunc(sf AccessValidationFunc) {
	r.secureFunc = sf
}

func (r *Restorator) selectMarshal(accept string) Marshall {
	m, ok := r.marshalls[accept]
	if !ok {
		return nil
	}
	return m
}

func (r *Restorator) selectController(action string) Controller {
	controller, ok := r.controllers[action]
	if !ok {
		return nil
	}
	return controller()
}

func (r *Restorator) prepareInput(req *http.Request, controller Controller, identifiers []int64, marshall Marshall) (input ControllerInput, err error) {
	input = NewControllerInput()
	if req.URL.Query().Get("limit") != "" {
		input.Limit = helpers.ParseInt64(req.URL.Query().Get("limit"))
	} else {
		input.Limit = 20
	}
	input.Offset = helpers.ParseInt64(req.URL.Query().Get("offset"))
	for _, header := range controller.RequeredHeader() {
		if len(req.Header[header]) == 0 {
			return input, fmt.Errorf("Requered header %v not found", header)
		}
		input.Headers[header] = req.Header[header]
	}
	for _, header := range controller.InterestedHeader() {
		input.Headers[header] = req.Header[header]
	}
	reqQuery := req.URL.Query()
	for _, query := range controller.RequeredQuery() {
		if len(reqQuery[query]) == 0 {
			return input, fmt.Errorf("Requered query %v not found", query)
		}
		input.Values[query] = reqQuery[query]
	}
	for _, query := range controller.InterestedQuery() {
		input.Values[query] = reqQuery[query]
	}
	if len(reqQuery["select"]) != 0 {
		qSelect := helpers.SplitString(req.URL.Query().Get("select"), ',')
		for _, qSel := range qSelect {
			for _, field := range controller.Fields() {
				if qSel == field {
					if len(input.Select) == 0 {
						input.Select = qSel
					} else {
						input.Select += ", " + qSel
					}
				}
			}
		}
	} else {
		input.Select = "*"
	}
	body := controller.GetBodyStruct()
	bodyByte, _ := ioutil.ReadAll(req.Body)
	defer req.Body.Close()
	marshall.Unmarshall(bodyByte, body)
	if req.Method == _POST {
		req.ParseMultipartForm(32 << 20)
		input.BodyFile.File, input.BodyFile.Header, _ = req.FormFile(controller.UploadFileName())
	}
	input.ReqBody = body
	input.Identifiers = identifiers
	return input, nil
}

func (r *Restorator) runController(Method string, input ControllerInput, controller Controller) (output ControllerOutput, err error) {
	switch Method {
	case _PUT:
		output, err = controller.Put(input)
	case _DELETE:
		output, err = controller.Delete(input)
	case _POST:
		output, err = controller.Post(input)
	default:
		output, err = controller.Get(input)
	}
	return output, err
}

func (r *Restorator) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	errorOutput := NewControllerOutput()
	errorOutput.Headers.Set(contentType, textPlain)
	if r.secureFunc != nil {
		if !r.secureFunc(req.URL.String(), req.Header.Get(xSecureHeader)) {
			errorOutput.set([]byte(noAccessStr), noAccessCode).write(res)
			return
		}
	}
	marshall := r.selectMarshal(req.Header.Get(accept))
	if marshall == nil {
		errorOutput.set([]byte(unknownMarshallStr), unknownMarshallCode).write(res)
		return
	}
	action := r.get_Actions(req.URL.Path)
	reqMethod := req.Method
	var err error
	controller := r.selectController(action.Name)
	if controller == nil {
		errorOutput.set([]byte(unknownControllerStr), unknownControllerCode).write(res)
		return
	}
	if !controller.Can(reqMethod) {
		errorOutput.set([]byte(methodNotAlowerErrorStr), methodNotAlowerErrorCode).write(res)
		return
	}
	input, err := r.prepareInput(req, controller, action.Identifiers, marshall)
	if err != nil {
		errorOutput.set([]byte(err.Error()), requeredNotSentErr).write(res)
		return
	}
	output, err := r.runController(reqMethod, input, controller)
	if err != nil {
		errorOutput.set([]byte(err.Error()), controllerErrorCode).write(res)
		return
	}
	result, err := marshall.Marshall(output.Data)
	if err != nil {
		errorOutput.set([]byte(err.Error()), marshallErrorCode).write(res)
		return
	}
	output.Data = result
	if output.Status == 0 {
		output.Status = _OK
	}
	output.Headers.Set(contentType, marshall.GetAcceptReqValue())
	output.write(res)
	return
}
