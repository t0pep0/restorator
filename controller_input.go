package restorator

import (
	"mime/multipart"
	"net/http"
	"net/url"
)

type ControllerInput struct {
	Headers     http.Header
	Values      url.Values
	Identifiers []int64
	ReqBody     interface{}
	BodyFile    struct {
		File   multipart.File
		Header *multipart.FileHeader
	}
	Select string
	Limit  int64
	Offset int64
}

func NewControllerInput() ControllerInput {
	ci := ControllerInput{}
	ci.Headers = http.Header(make(map[string][]string))
	ci.Values = url.Values(make(map[string][]string))
	return ci
}
