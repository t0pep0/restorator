package helpers

import (
	"strconv"
	"testing"
)

func TestParseInt64(t *testing.T) {
	for i := -999; i < 999; i++ {
		str := strconv.Itoa(i)
		if int64(i) != ParseInt64(str) {
			t.Errorf("Result mismatch! %i v= %v", i, ParseInt64(str))
		}
	}
}

func TestSlpitString(t *testing.T) {
	var table = map[string][]string{
		"/a/b/c/d/e":       []string{"a", "b", "c", "d", "e"},
		"a/b/c/d e/":       []string{"a", "b", "c", "d e"},
		"/a/b/c/d/e/":      []string{"a", "b", "c", "d", "e"},
		"/a/b/c////d/e":    []string{"a", "b", "c", "d", "e"},
		"/a////b////c/d/e": []string{"a", "b", "c", "d", "e"},
		"/abcde/":          []string{"abcde"},
	}
	for arg, res := range table {
		if len(SplitString(arg, '/')) != len(res) {
			t.Errorf("Result mismatch! %v != %v (%v)", SplitString(arg, '/'), res, arg)
		}
		for i, s := range SplitString(arg, '/') {
			if s != res[i] {
				t.Errorf("Result mismatch! %v != %v (%v)", SplitString(arg, '/'), res, arg)
			}
		}
	}
}

func TestSplitInt64Value(t *testing.T) {
	var table = map[string][]int64{
		"1,2,3,4,5,6,7,8":   []int64{1, 2, 3, 4, 5, 6, 7, 8},
		"12,345,67,-8":      []int64{12, 345, 67, -8},
		"1,2,3,4,5,6,,7,8":  []int64{1, 2, 3, 4, 5, 6, 7, 8},
		"1,2,3,4,5,6,7,8,":  []int64{1, 2, 3, 4, 5, 6, 7, 8},
		",1,2,3,4,5,6,7,8":  []int64{0, 1, 2, 3, 4, 5, 6, 7, 8},
		",1,2,3,4,5,6,7,8,": []int64{0, 1, 2, 3, 4, 5, 6, 7, 8},
		"1,2,3,4,5,6,-0,8":  []int64{1, 2, 3, 4, 5, 6, 0, 8},
		"12345678":          []int64{12345678},
		",12345678,":        []int64{0, 12345678},
	}
	for arg, res := range table {
		if len(SplitInt64Value(arg, ',')) != len(res) {
			t.Errorf("Result mismatch! %v != %v (%v)", SplitInt64Value(arg, ','), res, arg)
		}
		for i, s := range SplitInt64Value(arg, ',') {
			if s != res[i] {
				t.Errorf("Result mismatch! %v != %v", SplitInt64Value(arg, ','), res)
			}
		}
	}
}

func BenchmarkParseInt64(b *testing.B) {
	for i := 0; i < b.N; i++ {
		ParseInt64("1234803422")
	}
}

func BenchmarkSplitString(b *testing.B) {
	for i := 0; i < b.N; i++ {
		SplitString("/a////b////c/d/e", '/')
	}
}

func BenchmarkSplitInt64Value(b *testing.B) {
	for i := 0; i < b.N; i++ {
		SplitInt64Value("/1////-40////2/5453/2534", '/')
	}
}
