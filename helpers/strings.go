package helpers

func SplitString(str string, sep byte) []string {
	res := make([]string, len(str)/2+1)
	count := 0
	li := 0
	i := 0
	for i = 0; i < len(str); i++ {
		if str[i] == sep {
			if li != i {
				res[count] = str[li:i]
				li = i + 1
				count++
			} else {
				li = i + 1
				continue
			}
		}
	}
	if li < i {
		res[count] = str[li:i]
		li = i + 1
		count++
	}
	return res[:count]
}

//If error - return zero value
func ParseInt64(str string) int64 {
	res := int64(0)
	negative := false
	for i := 0; i < len(str); i++ {
		if str[i] > 0x2F && str[i] < 0x3A {
			res *= 10
			res += int64(str[i] - 0x30)
		} else {
			if str[i] == 0x2D && i == 0 {
				negative = true
			} else {
				return 0
			}
		}
	}
	if negative {
		return -res
	}
	return res
}

func SplitInt64Value(str string, sep byte) []int64 {
	res := make([]int64, len(str)/2+1)
	count := 0
	negative := false
	for i := 0; i < len(str); i++ {
		if str[i] > 0x2F && str[i] < 0x3A {
			res[count] *= 10
			res[count] += int64(str[i] - 0x30)
		} else if str[i] == sep {
			if i < len(str)-1 {
				if str[i+1] == sep {
					continue
				}
			} else {
				break
			}
			if negative {
				res[count] = -res[count]
				negative = !negative
			}
			count++
		} else if str[i] == 0x2D {
			if str[i-1] == sep {
				negative = true
			}
		}
	}
	if negative {
		res[count] = -res[count]
	}
	count++
	return res[:count]
}
